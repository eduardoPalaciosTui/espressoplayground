package com.example.espressoplayground

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        replaceFragment(LoginFragment())
    }
}

fun FragmentActivity.replaceFragment(fragment: Fragment, addToBackStack: Boolean = false) {
    val transaction = supportFragmentManager.beginTransaction().replace(R.id.container, fragment)
    if (addToBackStack) {
        transaction.addToBackStack(fragment::class.java.simpleName)
    }
    transaction.commit()
}

class LoginFragment : Fragment() {

    private var userName: String? = null
    private var password: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val findViewById = view.findViewById<EditText>(R.id.user_name)
        findViewById.addTextChangedListener({ _, _, _, _ -> }, { _, _, _, _ -> }, { userName = it.toString() })
        view.findViewById<EditText>(R.id.password).addTextChangedListener({ _, _, _, _ -> }, { _, _, _, _ -> }, { password = it.toString() })
        view.findViewById<Button>(R.id.login_button).setOnClickListener {
            if (userName.isNullOrBlank() or password.isNullOrBlank()) {
                Toast.makeText(requireContext(), "Invalid credentials", Toast.LENGTH_LONG).show()
            } else {
                InMemoryRepository.saveUserCredentials(userName)
                activity?.replaceFragment(DetailsFragment(), addToBackStack = true)
            }
        }
    }

}

class DetailsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val welcomeMessage = "Welcome ${InMemoryRepository.userName}"
        view.findViewById<TextView>(R.id.welcome_user_name).text = welcomeMessage
    }

}

object InMemoryRepository {

    var userName: String? = null
        private set

    fun saveUserCredentials(userName: String?) {
        this.userName = userName
    }
}
